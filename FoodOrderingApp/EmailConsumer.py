import json
from confluent_kafka import Consumer

ORDER_CONFIRMED_KAFKA_TOPIC = "order_confirmed10"

email_consumer_config = {
    "group.id": "3",
    'bootstrap.servers': 'localhost:9092',
    'auto.offset.reset': 'earliest'
}
consumer = Consumer(**email_consumer_config)
consumer.subscribe([ORDER_CONFIRMED_KAFKA_TOPIC])

emails_sent_so_far = set()
print("Email start listening")

while True:
    # to retrieve records one-by-one
    message = consumer.poll(timeout=1.0)
    if message is None:
        continue
    if message:
        print(message.value())
        print(f"message is consumed from the partition {message.partition()}; Offset - {message.offset()}")

    consumed_message = json.loads(message.value())
    customer_email = consumed_message["customer_email"]
    print(f"Sending email to {customer_email} ")
    emails_sent_so_far.add(customer_email)
    print(f"So far emails sent to {len(emails_sent_so_far)} unique emails")

consumer.close()
