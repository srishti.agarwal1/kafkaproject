import json

from confluent_kafka import Consumer

ORDER_CONFIRMED_KAFKA_TOPIC = "order_confirmed10"

analytics_consumer_config = {
    "group.id": "3",
    'bootstrap.servers': 'localhost:9092',
    'auto.offset.reset': 'earliest'
}

consumer = Consumer(**analytics_consumer_config)
consumer.subscribe([ORDER_CONFIRMED_KAFKA_TOPIC])

total_orders_count = 0
total_revenue = 0
print("Analytics start listening")
while True:
    message = consumer.poll(timeout=1.0)
    if message is None:
        continue
    if message:
        print(message.value())
        print(f"message is consumed from the partition {message.partition()}; Offset - {message.offset()}")

    consumed_message = json.loads(message.value())
    total_cost = float(consumed_message["total_cost"])
    total_orders_count += 1
    total_revenue += total_cost
    print(f"Orders so far today: {total_orders_count}")
    print(f"Revenue so far today: {total_revenue}")
