"""The Python implementation of the gRPC ordering app server."""
import json
import os
from concurrent import futures
import psycopg2
import logging

import grpc

import orders_pb2
import orders_pb2_grpc
from confluent_kafka import Producer

ORDER_KAFKA_TOPIC = "order_details10"
ORDER_CONFIRMED_KAFKA_TOPIC = "order_confirmed10"

producer_config = {
    "bootstrap.servers": "localhost:9092",
}

producer = Producer(producer_config)


def delivery_report(err, msg):
    if err is not None:
        print("Failed to deliver message: %s: %s" % (msg.value(), str(err)))
    else:
        print("Message produced: %s" % (msg.value()))
        print(f"message is delivered to the partition {msg.partition()}; Offset - {msg.offset()}")


def get_connection():
    con = psycopg2.connect(
        database="postgres", user='', password='', host='localhost', port='5432'
    )
    return con


print("Transaction start listening")


class OrderServicer(orders_pb2_grpc.OrdersServicer):
    """Provides methods that implement functionality of orderingApp server."""

    def __init__(self):
        self._bootstrap_servers = os.environ.get("BOOTSTRAP_SERVERS", "localhost:9092")
        self._producer = None
        self.orders_list = []

    def _get_producer(self):
        config = {
            "bootstrap.servers": self._bootstrap_servers,
        }
        self._producer = Producer(config)
        return True

    def SaveNewOrder(self, request, context):
        if self._producer is None:
            self._get_producer()
        order_details_message = request
        ack = f'Hello I am up and running received "{order_details_message}" message from you'
        result = {'wasSaved': True, 'orderId': request.order_id, 'message': ack}
        data = {
            "order_id": request.order_id,
            "user_id": request.user_id,
            "total_cost": request.total_cost,
            "items": request.items,
        }
        if result is None:
            return orders_pb2.OrderResponse(wasSaved=False, orderId=0, ack="")
        else:
            self.orders_list.append(data)
            self._producer.produce(ORDER_KAFKA_TOPIC, value=json.dumps(data).encode("utf-8"),
                                   on_delivery=delivery_report)
            self._producer.poll(timeout=1)
            self._producer.flush()
            return orders_pb2.OrderResponse(**result)

    def ConfirmedOrdersFromDB(self, request, context):
        conn = get_connection()
        cursor = conn.cursor()
        cursor.execute('''SELECT * from confirmed_orders''')

        # transform result
        columns = list(cursor.description)
        result = cursor.fetchall()

        # make dict
        results = []
        for row in result:
            row_dict = {}
            for i, col in enumerate(columns):
                row_dict[col.name] = row[i]
            results.append(row_dict)

        conn.commit()
        cursor.close()
        conn.close()

        confirmed_results = []
        for data in results:
            confirmed_orders = orders_pb2.DBConfirmedOrders()
            result_order = orders_pb2.Result()

            setattr(result_order, "customer_id",  data["customer_id"])
            setattr(result_order, "customer_email",  data["customer_email"])
            setattr(result_order, "total_cost",  data["total_cost"])
            setattr(result_order, "topic",  data["topic"])

            confirmed_results.append(result_order)
            confirmed_orders.result.extend(confirmed_results)
        return confirmed_orders


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    orders_pb2_grpc.add_OrdersServicer_to_server(
        OrderServicer(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    serve()
