import json

from confluent_kafka import Consumer
from confluent_kafka import Producer
import psycopg2

ORDER_KAFKA_TOPIC = "order_details10"
ORDER_CONFIRMED_KAFKA_TOPIC = "order_confirmed10"
test_topic = "test_topic1"

consumer_conf = {
    "group.id": "3",
    'bootstrap.servers': 'localhost:9092',
    'auto.offset.reset': 'earliest'
}

consumer = Consumer(**consumer_conf)
consumer.subscribe([ORDER_KAFKA_TOPIC])

producer_config = {
    "bootstrap.servers": "localhost:9092",
}

producer = Producer(producer_config)


def delivery_report(err, msg):
    if err is not None:
        print("Failed to deliver message: %s: %s" % (msg.value(), str(err)))
    else:
        print("Message produced: %s" % (msg.value()))
        print(f"message is delivered to the partition {msg.partition()}; Offset - {msg.offset()}")


def get_connection():
    con = psycopg2.connect(
        database="postgres", user='', password='', host='localhost', port='5432'
    )
    return con


print("Transaction start listening")
while True:
    # to retrieve records one-by-one
    message = consumer.poll(timeout=1.0)
    if message is None:
        continue
    if message:
        print(message.value())
        print(f"message is consumed from the partition {message.partition()}; Offset - {message.offset()}")

    consumed_message = json.loads(message.value())
    user_id = consumed_message["user_id"]
    total_cost = consumed_message["total_cost"]
    customerEmail = f"{user_id}@gmail.com"
    data = {
        "customer_id": user_id,
        "customer_email": f"{user_id}@gmail.com",
        "total_cost": total_cost
    }

    # storing data into postgres
    conn = get_connection()
    cursor = conn.cursor()
    cursor.execute("INSERT INTO confirmed_orders(customer_id, customer_email, total_cost, topic) "
                   "VALUES(%s,%s,%s,%s)", (user_id, customerEmail, total_cost, ORDER_CONFIRMED_KAFKA_TOPIC,))
    conn.commit()
    cursor.close()
    conn.close()

    print("Successful transaction..")
    producer.produce(ORDER_CONFIRMED_KAFKA_TOPIC, value=json.dumps(data).encode("utf-8"), on_delivery=delivery_report)
    producer.poll(timeout=1)
    producer.flush()

consumer.close()
